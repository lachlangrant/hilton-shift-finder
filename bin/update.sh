#!/bin/bash

git reset --hard
git pull

npm i

pm2 stop all
pm2 start ecosystem.config.js
pm2 startup
