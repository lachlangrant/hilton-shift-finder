module.exports = {
	apps: [{
		name: 'HiltonShiftFinder',
		script: 'src/index.js',
		instances: 1,
		autorestart: true,
		watch: false,
		max_memory_restart: '1G',
		ignore_watch: ["node_modules", "config.json", "views"],
		env: {
			NODE_ENV: 'development'
		},
		env_production: {
			NODE_ENV: 'production'
		}
	}],
};
