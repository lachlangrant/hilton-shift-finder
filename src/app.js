const express = require('express');
const app = express();

const server = require('http').Server(app);
const io = require('socket.io')(server);
const { exec } = require("child_process");

io.on('connection', function (socket) {
	socket.emit('settings', settings);
	socket.on('pm2reload', function (data) {
		process.exit(0);
	});

	socket.on('update', async function (data) {
		exec("./bin/update.sh");
	});
});

process.stdin.resume();

var settings = require('./settings').settings;
const saveSettings = require('./settings').saveSettings;

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.render('home', {
		daysToAccept: settings.daysToAccept,
		minShiftLength: settings.minShiftLength,
		maxShiftLength: settings.maxShiftLength,
		delay: settings.delay,
		timeout: settings.timeout,
		background: settings.background,
	});
});

app.post('/day/:day/remove', (req, res) => {
	settings.daysToAccept.splice(settings.daysToAccept.indexOf(req.params.day), 1);
	saveSettings();
	res.redirect('/');
});

app.post('/addDay', (req, res) => {
	settings.daysToAccept.push(req.body.day.toString().trim());
	saveSettings();
	res.redirect('/');
});

app.post('/delay', (req, res) => {
	settings.delay = req.body.delay;
	saveSettings();
	res.redirect('/');
});

app.post('/minlength', (req, res) => {
	settings.minShiftLength = req.body.length;
	saveSettings();
	res.redirect('/');
});

app.post('/maxlength', (req, res) => {
	settings.maxShiftLength = req.body.length;
	saveSettings();
	res.redirect('/');
});

app.post('/timeout', (req, res) => {
	if (req.body.timeout >= 100) {
		settings.timeout = req.body.timeout;
		saveSettings();
		res.redirect('/');
	} else {
		res.json({ message: "timeout too small" });
	}
});

module.exports = { app, server, io };
