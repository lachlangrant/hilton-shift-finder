const fs = require('fs');
const path = require('path');

if (!fs.existsSync(path.join(__dirname, "../config.json"))) {
	console.error("Please create a config.json file");
	console.error("Refer to README for example");
	process.exit();
} else {
	const settings = require('./settings').settings;

	if (settings.username === undefined || settings.username === null || settings.password === undefined || settings.password === null) {
		console.error("Missing Username and/or Password");
		process.exit();
	}
}

const app = require('./app');
const Networking = require('./networking');
const system = require('./system');

app.server.listen(3000);

const networking = new Networking();
networking.initChrome();
system.startLoop();