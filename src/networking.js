const puppeteer = require('puppeteer');
const moment = require('moment');
const io = require('./app').io;
const settingsLib = require('./settings');
const timeoutPromise = require("./timeout");
const timeout = ms => new Promise(res => setTimeout(res, ms));

const settings = settingsLib.settings;

const userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/112.0";

class Networking {
	constructor() {
		this.loginURL = "https://once.deputy.com/my/login";

		this.browser = null;
		this.deputyAuthCookie = null;

		this.startString = moment(Date.now())
			.format("YYYY-MM-DD HH:mm:SSSS ZZ");
		this.running = false;
	}

	log(message) {
		if (settings.console == true) {
			console.log(message);
		}
		io.emit("log", { "message": message });
	}

	async initChrome() {
		let start = Date.now();
		this.running = false;
		this.log("Starting new Browser...");

		if (settings.overrideChrome) {
			this.browser = await puppeteer
				.launch({ headless: true })
				.catch(e => console.error(e));
		} else {
			this.browser = await puppeteer
				.launch({
					args: ['--no-sandbox', '--single-process'],
					executablePath: '/usr/bin/chromium-browser'
				})
				.catch(e => console.error(e));
		}

		if (settings.debugWeb !== true) {
			process.on('exit', this.exitHandler);
			process.on('SIGINT', this.exitHandler);
			process.on('SIGUSR1', this.exitHandler);
			process.on('SIGUSR2', this.exitHandler);
			process.on('uncaughtException', this.exitHandler);
		}
		await this.getCookie();
		let end = Date.now();
		this.log(`Time to get cookie: ${end - start} ms`);
	}

	async getCookie() {
		if (this.browser === undefined || this.browser === null) {
			await this.initChrome();
		}

		this.log("Starting Cookie");
		const cookiePage = await this.browser.newPage();
		await cookiePage.setUserAgent(userAgent);

		this.log("Loading Cookie Page");
		await cookiePage.goto(this.loginURL, { waitUntil: 'networkidle2' });

		if (await cookiePage.$('#my-dashboard')) {
			this.log("Old cookie is still valid");
			await cookiePage.close();
		} else {
			this.log("Filling out form...");
			await cookiePage.type('#login-email', settings.username);
			await cookiePage.type('#login-password', settings.password);
			this.log("Submitting form...");
			await cookiePage.click('[name="btnLoginSubmit"]');
			this.log("Waiting for login complete...");
			await cookiePage.waitForNavigation({ waitUntil: 'networkidle0' });

			this.log("Reading cookies...");
			const cookies = await cookiePage.cookies();
			await cookiePage.close();

			cookies.map(cookie => {
				if (cookie.name === "DPSID") {
					this.deputyAuthCookie = cookie.value;
					this.log("Authenticated with Deputy, Cookie Found");
					this.start();
				}
			});
		}

		await this.browser.close();
		this.log("Chrome Stopped");
		// this.start();
	}

	async exitHandler() {
		if (this.browser) {
			try {
				this.log("Killing Chrome");
				await this.browser.close();
				process.exit();
			} catch (e) {
				this.log("Unable to kill chrome... try and restart the pi...");
				process.exit();
			}
		} else {
			this.log("Chrome is already stopped?");
			process.exit();
		}
	}


	findAShift() {
		if (settings.timeout === undefined) {
			settings.timeout = 15 * 1000;
			set.saveSettings();
		}
		let promises = [
			timeoutPromise(settings.timeout, this.processBody()),
		];
		return Promise.all(promises);
	}

	async processBody() {
		if (this.deputyAuthCookie !== undefined) {
			const today = moment(Date.now())
				.format("YYYY-MM-DD");
			const nextWeek = moment(Date.now()).add(14, "days").format("YYYY-MM-DD");

			var myHeaders = new Headers();
			myHeaders.append("User-Agent", userAgent);
			myHeaders.append("Accept", "*/*");
			myHeaders.append("Accept-Language", "en-US,en;q=0.5");
			myHeaders.append("Accept-Encoding", "gzip, deflate, br");
			myHeaders.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			myHeaders.append("Origin", `https://${settings.url}`);
			myHeaders.append("Connection", "keep-alive");
			myHeaders.append("Referer", `https://${settings.url}`);
			myHeaders.append("Cookie", `dp_logged_in=${settings.url};DPSID=${this.deputyAuthCookie}`);

			var requestOptions = {
				method: 'POST',
				headers: myHeaders,
				body: JSON.stringify({
					"open": {
						"url": `my/roster/open/${today}/${nextWeek}`,
						"method": "GET",
					}
				}),
				redirect: 'follow'
			};


			const res = await fetch(
				`https://${settings.url}/api/v1/multi`,
				requestOptions,
			);

			if (!res.ok) {
				this.deputyAuthCookie = undefined;
				this.initChrome();
				this.log("Deputy response wasn't ok: " + res.statusText);
				return Promise.resolve();
			}

			try {
				const json = await this.safeParseJSON(res);
				if (json['open'] !== undefined) {
					const shifts = json['open'];

					if (shifts === undefined) {
						return Promise.resolve();
					}

					let promises = shifts.map((i) => this.processShift(i));

					await Promise.all(promises);
					return Promise.resolve();
				} else {
					if (json['error'] !== undefined && json['error'] !== null) {
						this.log(`Message From Deputy: ${json['error']['message']}`);
					}
					this.log("Deputy didn't return expected data...");
					this.deputyAuthCookie = undefined;
					this.initChrome();
					return Promise.resolve();
				}

			} catch (e) {
				this.log(e.toString());
				return Promise.resolve();
			}
		} else {
			this.log("No Cookie!!!");
			return Promise.resolve();
		}
	}

	async safeParseJSON(response) {
		const txt = await response.text();
		if (response.status !== 200) {
			this.log("Deputy response was not successful.");
		}
		if (txt.indexOf("<html>") > -1) {
			this.log("Deputy response was not json.");
		}

		try {
			const unwrapped = JSON.parse(txt);
			return unwrapped;
		} catch (err) {
			this.log("Deputy response was not what we were expecting");
		}
	}

	async processShift(shift) {
		const dayOfTheWeek = moment.parseZone(shift.Date).format("dddd");

		if (settings.daysToAccept.indexOf(dayOfTheWeek) > -1) {
			this.log("Found shift on appropiate day: " + dayOfTheWeek);
			if (shift.TotalTime >= settings.minShiftLength && shift.TotalTime <= settings.maxShiftLength) {
				this.log("Found shift in range");
			} else {
				this.log("Shift was too short or too long at " + shift.TotalTime + " hours.");
				return;
			}

			var myHeaders = new Headers();
			myHeaders.append("User-Agent", userAgent);
			myHeaders.append("Accept", "*/*");
			myHeaders.append("Accept-Language", "en-US,en;q=0.5");
			myHeaders.append("Accept-Encoding", "gzip, deflate, br");
			myHeaders.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			myHeaders.append("Origin", `https://${settings.url}`);
			myHeaders.append("Connection", "keep-alive");
			myHeaders.append("Referer", `https://${settings.url}`);
			myHeaders.append("Cookie", `dp_logged_in=${settings.url};DPSID=${this.deputyAuthCookie}`);

			var requestOptions = {
				method: 'POST',
				headers: myHeaders,
				body: JSON.stringify({
					"intRosterArray": [shift.Id],
				}),
				redirect: 'follow'
			};

			fetch(
				`https://${settings.url}/api/v1/my/roster/open`,
				requestOptions,
			);

			this.log("Shift should have been accepted");
			return acceptRequest;
		} else {
			this.log(`Found shift on '${dayOfTheWeek}' which is not a day we're looking for.`);
			return Promise.resolve();
		}
	}

	async start() {
		if (this.running === true) {
			return Promise.resolve();
		}

		if (this.browser === undefined || this.browser === null) {
			await this.initChrome();
		}

		this.running = true;
		while (this.running) {
			try {
				let start = Date.now();
				await this.findAShift();
				let end = Date.now();
				this.log(`Time to process shifts: ${end - start} ms`);
				await timeout(settings.delay * 1000);
			} catch (e) {
				this.log("Crashed, checking again without delay");
				this.log(e.toString());
			}
		}
	}
}

module.exports = Networking;
