var settings = require("../config.json");
const fs = require('fs');
const path = require('path');

function saveSettings() {
	fs.writeFileSync(path.join(__dirname, '../config.json'), JSON.stringify(settings, null, 2));
}

if (settings.daysToAccept === undefined) {
	settings.daysToAccept = [];
	saveSettings();
}

module.exports = { settings, saveSettings };
