const io = require('./app').io;
const si = require('systeminformation');

function startLoop() {
	sendInfo();
	setInterval(sendInfo, 5 * 1000);
}

function sendInfo() {
	getCPULoad();
	getMemoryInMB();
}

function getCPULoad() {
	si.currentLoad().then((load) => {
		io.emit('cpu', load.currentload);
	});
}

function getMemoryInMB() {
	si.mem().then((memory) => {
		io.emit('memory', memory.active / 1000000);
	});
}

module.exports = {
	startLoop,
};